﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//----------
namespace Oenik_prog3_2016osz_ii4slt
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for MainWindow.xml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }     

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point mousePos = Mouse.GetPosition(this);
                (Content as Game).OnCLick((int)mousePos.X / Game.ITEMSIZE, (int)mousePos.Y / Game.ITEMSIZE);
            }
        }
    }
}
