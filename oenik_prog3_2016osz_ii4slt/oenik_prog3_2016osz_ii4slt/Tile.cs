﻿//-----------------------------------------------------------------------
// <copyright file="Tile.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//----------



namespace Oenik_prog3_2016osz_ii4slt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Windows.Media;
    
    /// <summary>
    /// Relevant informations for the tile items.
    /// </summary>
    public class Tile
    {
        private static Random rng = new Random();

        public Tile(int locX, int locY)
        {
            this.Color = rng.Next(0, 5);
            this.Loc = new int[2] { locX, locY };
        }

        public int Color { get; set; }

        public int[] Loc { get; set; }

        /// <summary>
        /// Returns a color based on the Tile.Color
        /// </summary>
        /// <param name="item">The color code of a Tile object.</param>
        /// <returns>Return a color.</returns>
        public static SolidColorBrush TileBrushColor(int item)
        {
            switch (item)
            {
                case 0:
                    return Brushes.Red;
                case 1:
                    return Brushes.Blue;
                case 2:
                    return Brushes.Green;
                case 3:
                    return Brushes.Yellow;
                case 4:
                    return Brushes.Purple;
                case 5:
                    return Brushes.Transparent;
                default:
                    return Brushes.White;
            }
        }
    }
}
