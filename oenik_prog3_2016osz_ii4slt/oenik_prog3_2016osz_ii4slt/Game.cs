﻿//-----------------------------------------------------------------------
// <copyright file="Game.cs" company="OE-NIK">
// No copyright (public domain)
// </copyright>
//-----------------------------------------------------------------------

namespace Oenik_prog3_2016osz_ii4slt
{
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Main class of the game
    /// </summary>
    public class Game : FrameworkElement
    {
        /// <summary>
        /// The size of the tiles used in several calculation.
        /// </summary>
        public const int ITEMSIZE = 40;

        private bool victory;

        private int stepCount;

        public Game()
        {
            this.Map = new Tile[10, 10];

            for (int i = 0; i < this.Map.GetLength(1); i++)
            {
                for (int j = 0; j < this.Map.GetLength(0); j++)
                {
                    do
                    {
                        this.Map[i, j] = new Tile(i, j);
                    }
                    while (this.CheckMatchStart(i, j));
                }
            }

            this.Points = 0;

            this.PreviousSelected = new Point(-1, -1);
            this.NewSelected = new Point(-1, -1);

            this.stepCount = 0;
            this.victory = false;
        }

        private int Points { get; set; }

        private Point PreviousSelected { get; set; }

        private Point NewSelected { get; set; }

        private Tile[,] Map { get; set; }

        /// <summary>
        /// Initiates if there's is a match of at least four of the same colored tiles.
        /// </summary>
        /// <param name="locX">X coordinate of the starting tile in the array.</param>
        /// <param name="locY">Y coordinate of the starting tile in the array.</param>
        /// <returns>Returns if there's at least 4 matching tiles.</returns>
        public bool CheckMatchStart(int locX, int locY)
        {
            int counter = 0;
            List<Tile> checkList = new List<Tile>();
            this.CheckMatch(locX, locY, this.Map[locX, locY].Color, ref counter, ref checkList);

            if (counter > 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Handles all relevant mouse clicks.
        /// </summary>
        /// <param name="posX">X position of mouse.</param>
        /// <param name="posY">Y position of mouse.</param>
        public void OnCLick(int posX, int posY)
        {
            if (!this.victory)
            {
                if (this.ValidSelection(this.PreviousSelected))
                {
                    if (((posX - this.PreviousSelected.X) + (posY - this.PreviousSelected.Y)) == -1 || ((posX - this.PreviousSelected.X) + (posY - this.PreviousSelected.Y)) == 1)
                    {
                        this.stepCount++;
                        this.NewSelected = new Point((int)posX, (int)posY);
                        if (this.ValidSelection(this.NewSelected))
                        {
                            int color = this.Map[(int)this.PreviousSelected.X, (int)this.PreviousSelected.Y].Color;
                            this.Map[(int)this.PreviousSelected.X, (int)this.PreviousSelected.Y].Color = this.Map[(int)this.NewSelected.X, (int)this.NewSelected.Y].Color;
                            this.Map[(int)this.NewSelected.X, (int)this.NewSelected.Y].Color = color;

                            bool[] succes = new bool[2] { false, false };

                            succes[0] = this.BreakTiles((int)this.PreviousSelected.X, (int)this.PreviousSelected.Y);
                            succes[1] = this.BreakTiles((int)this.NewSelected.X, (int)this.NewSelected.Y);

                            

                            if (!succes[0] && !succes[1])
                            {
                                this.Map[(int)this.NewSelected.X, (int)this.NewSelected.Y].Color = this.Map[(int)this.PreviousSelected.X, (int)this.PreviousSelected.Y].Color;
                                this.Map[(int)this.PreviousSelected.X, (int)this.PreviousSelected.Y].Color = color;
                            }
                        }

                        this.NewSelected = new Point(-1, -1);
                        this.PreviousSelected = new Point(-1, -1);

                        if (this.Points >= 1000)
                        {
                            this.victory = true;
                            this.Points = 1000;
                            this.InvalidateVisual();
                            if (this.victory)
                            {
                                MessageBox.Show("Győztél! " + this.stepCount.ToString() + " lépésből sikerült elérned a szükséges pontszámot.");
                            }
                        }
                    }
                    else
                    {
                        this.PreviousSelected = new Point(-1, -1);
                    }
                }
                else
                {
                    this.PreviousSelected = new Point((int)posX, (int)posY);
                }

                this.InvalidateVisual();
            }
        }

        /// <summary>
        /// Generates the visual that's to be drawn.
        /// </summary>
        /// <param name="drawingContext">The drawing context.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(Brushes.White, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));

            drawingContext.DrawRectangle(Brushes.Green, new Pen(Brushes.Black, 2), new Rect(12 * ITEMSIZE, 50 + ((1000 - this.Points) / 5), 40, this.Points / 5));

            drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Black, 2), new Rect(12 * ITEMSIZE, 50, 40, 200));

            for (int i = 0; i < this.Map.GetLength(0); i++)
            {
                for (int j = 0; j < this.Map.GetLength(1); j++)
                {
                    drawingContext.DrawRectangle(Tile.TileBrushColor(this.Map[j, i].Color), new Pen(Brushes.Black, 1), new Rect(j * ITEMSIZE, i * ITEMSIZE, ITEMSIZE, ITEMSIZE));
                }
            }

            if (this.ValidSelection(this.PreviousSelected))
            {
                drawingContext.DrawRectangle(   
                    Tile.TileBrushColor(this.Map[(int)this.PreviousSelected.X, (int)this.PreviousSelected.Y].Color), 
                    new Pen(Brushes.Red, 1),
                    new Rect((int)this.PreviousSelected.X * ITEMSIZE, (int)this.PreviousSelected.Y * ITEMSIZE, ITEMSIZE, ITEMSIZE));
            }
        }

        private void CheckMatch(int locX, int locY, int originalColor, ref int counter, ref List<Tile> checkTiles)
        {
            if (this.Map[locX, locY] != null && this.Map[locX, locY].Color != -1 && !checkTiles.Contains(this.Map[locX, locY]) && this.Map[locX, locY].Color == originalColor)
            {
                counter++;
                checkTiles.Add(this.Map[locX, locY]);
                if (locX + 1 != 10)
                {
                    this.CheckMatch(locX + 1, locY, originalColor, ref counter, ref checkTiles);
                }

                if (locX - 1 != -1)
                {
                    this.CheckMatch(locX - 1, locY, originalColor, ref counter, ref checkTiles);
                }

                if (locY + 1 != 10)
                {
                    this.CheckMatch(locX, locY + 1, originalColor, ref counter, ref checkTiles);
                }

                if (locY - 1 != -1)
                {
                    this.CheckMatch(locX, locY - 1, originalColor, ref counter, ref checkTiles);
                }
            }   
        }

        private bool ValidSelection(Point point)
        {
            return point.X >= 0 && point.Y >= 0 && point.X < 10 && point.Y < 10;
        }

        private bool BreakTiles(int locX, int locY)
        {
            if (this.CheckMatchStart(locX, locY))
            {
                int counter = 0;
                List<Tile> checkList = new List<Tile>();
                List<int[]> pointList = new List<int[]>();

                this.CheckMatch(locX, locY, this.Map[locX, locY].Color, ref counter, ref checkList);

                foreach (Tile item in checkList)
                {
                    pointList.Add(item.Loc);
                }

                pointList.Sort((int[] x, int[] y) => x[1].CompareTo(y[1]));

                foreach (Tile item in checkList)
                {
                    this.Map[item.Loc[0], item.Loc[1]].Color = -1;
                }

                foreach (int[] item in pointList)
                {
                    this.MoveTile(item[0], item[1]);
                    for (int i = item[1]; i >= 0; i--)
                    {
                        this.BreakTiles(item[0], i);
                    }
                }

                if (counter > 3)
                {
                    this.Points += counter * 2;
                }

                this.InvalidateVisual();
                return true;
            }

            return false;
        }

        private void MoveTile(int locX, int locY)
        {
            int currentY = locY;
            int currentEmpty = locY;

            while (currentY >= 0 && this.Map[locX, currentY].Color == -1)
            {
                currentY--;
            }

            if (currentY >= 0 && this.Map[locX, currentY].Color != -1)
            {
                while (currentY >= 0)
                {
                    this.Map[locX, currentEmpty].Color = this.Map[locX, currentY].Color;
                    currentEmpty--;
                    currentY--;
                }
            }

            while (currentEmpty >= 0)
            {
                this.Map[locX, currentEmpty] = new Tile(locX, currentEmpty);
                currentEmpty--;
            }
        }
    }
}
